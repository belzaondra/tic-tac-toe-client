import { Box, Text } from "@chakra-ui/react";
import { FC } from "react";

const Footer: FC = () => {
  return (
    <>
      <Box bg="blue.400" color="white">
        <Text textAlign="center">Made by Ondrej Belza</Text>
      </Box>
    </>
  );
};
export default Footer;
