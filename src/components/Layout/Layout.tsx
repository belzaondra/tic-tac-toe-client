import { Box, Flex } from "@chakra-ui/react";
import { FC } from "react";
import Footer from "./Footer";
import Header from "./Header";

const Layout: FC = ({ children }) => {
  return (
    <>
      <Flex direction="column" minH="100vh">
        <Header />
        <Box flexGrow={1}>{children}</Box>
        <Footer />
      </Flex>
    </>
  );
};

export default Layout;
