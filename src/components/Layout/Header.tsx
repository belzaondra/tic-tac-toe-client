import {
  Box,
  Button,
  Flex,
  Icon,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import React, { FC } from "react";
import {
  BiLogIn,
  BiLogOut,
  BiPlus,
  BiRefresh,
  GiHamburgerMenu,
  MdVideogameAsset,
} from "react-icons/all";
import { Link } from "react-router-dom";

const Header: FC = () => {
  return (
    <>
      <Box bg="blue.400" w="100%" p={4} color="white" flex="row">
        <Flex alignItems="center">
          <Box display="flex" as={Link} to="/">
            <Text fontSize="2xl" mr={2} fontWeight="bold">
              Tic tac toe
            </Text>
            <Icon as={MdVideogameAsset} w={8} h={8} mt={1} />
          </Box>

          <Box ml="auto" display={{ base: "block", md: "none" }} mr={2}>
            <Menu>
              <MenuButton as={GiHamburgerMenu} aria-label="Menu" />
              <MenuList color="black">
                <MenuItem icon={<BiLogIn />} as={Link} to="/login">
                  Login
                </MenuItem>
                <MenuItem icon={<BiPlus />} as={Link} to="/register">
                  Register
                </MenuItem>
                <MenuItem icon={<BiRefresh />} as={Link} to="/forgotPassword">
                  Reset password
                </MenuItem>
                <MenuItem icon={<BiLogOut />} as={Link} to="/logout">
                  Logout
                </MenuItem>
              </MenuList>
            </Menu>
          </Box>

          <Box ml="auto" display={{ base: "none", md: "block" }}>
            <Button
              as={Link}
              to="/register"
              variant="link"
              mx={2}
              color="white"
            >
              Register
            </Button>
            <Button as={Link} to="/login" variant="link" mx={2} color="white">
              Login
            </Button>
            <Button
              as={Link}
              to="/forgotPassword"
              variant="link"
              mx={2}
              color="white"
            >
              Forgot password
            </Button>
            <Button as={Link} to="/logout" variant="link" mx={2} color="white">
              Logout
            </Button>
          </Box>
        </Flex>
      </Box>
    </>
  );
};

export default Header;
