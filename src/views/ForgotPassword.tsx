import {
  Box,
  Button,
  Center,
  Flex,
  FormControl,
  FormLabel,
  Image,
  Input,
  Text,
} from "@chakra-ui/react";
import { FC } from "react";

const ForgotPassword: FC = () => {
  return (
    <>
      <Flex justify="center" align="center">
        <Box
          borderWidth={1}
          shadow="md"
          borderRadius={5}
          mt={4}
          maxW="sm"
          w="sm"
        >
          <Center>
            <Image
              my={4}
              src="/images/password.svg"
              w="80%"
              alt="registration image"
            />
          </Center>
          <Text fontSize="2xl" textAlign="center" fontWeight="bold">
            Reset password
          </Text>
          <Center my={4}>
            <Box w="80%">
              <form>
                <FormControl id="email">
                  <FormLabel>Email address</FormLabel>
                  <Input type="email" />
                </FormControl>

                <Button
                  type="submit"
                  bg="blue.400"
                  color="white"
                  w="100%"
                  mt={4}
                >
                  Reset password
                </Button>
              </form>
            </Box>
          </Center>
        </Box>
      </Flex>
    </>
  );
};

export default ForgotPassword;
