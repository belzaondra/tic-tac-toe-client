import { Button, Center, Flex, Image, Text } from "@chakra-ui/react";
import { FC } from "react";
import { Link } from "react-router-dom";
const Index: FC = () => {
  return (
    <>
      <Text textAlign="center" fontSize="4xl" fontWeight="bold" my={4}>
        Tic tac toe
      </Text>
      <Text textAlign="center" fontSize="3xl" fontWeight="bold" my={4}>
        Invite your friends for online game of tic tac toe.
      </Text>
      <Center my={4}>
        <Image src="/images/winners.svg" alt="Winners photo" />
      </Center>

      <Flex justify="center">
        <Button mr={6} size="lg" as={Link} to="/login">
          Login
        </Button>
        <Button size="lg" as={Link} to="/register">
          Register
        </Button>
      </Flex>
    </>
  );
};
export default Index;
