import { Alert, AlertIcon } from "@chakra-ui/react";
import { FC } from "react";

const Logout: FC = () => {
  return (
    <>
      <Alert status="info">
        <AlertIcon />
        This page will just trigger logout and then redirect user to index page
      </Alert>
    </>
  );
};

export default Logout;
