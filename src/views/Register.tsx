import { FC } from "react";
import {
  Box,
  Flex,
  Text,
  Image,
  Center,
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Button,
} from "@chakra-ui/react";
const Registration: FC = () => {
  return (
    <>
      <Flex justify="center" align="center">
        <Box
          borderWidth={1}
          shadow="md"
          borderRadius={5}
          mt={4}
          maxW="sm"
          w="sm"
        >
          <Center>
            <Image
              my={4}
              src="/images/users.svg"
              w="80%"
              alt="registration image"
            />
          </Center>
          <Text fontSize="2xl" textAlign="center" fontWeight="bold">
            Registration
          </Text>
          <Center my={4}>
            <Box w="80%">
              <form>
                <FormControl id="email">
                  <FormLabel>Email address</FormLabel>
                  <Input type="email" />
                  <FormHelperText>We'll never share your email.</FormHelperText>
                </FormControl>

                <FormControl id="password" mt={4}>
                  <FormLabel>Password</FormLabel>
                  <Input type="password" />
                </FormControl>

                <FormControl id="passwordConfirmation" mt={4}>
                  <FormLabel>Password confirmation</FormLabel>
                  <Input type="password" />
                </FormControl>

                <Button
                  type="submit"
                  bg="blue.400"
                  color="white"
                  w="100%"
                  mt={4}
                >
                  Register
                </Button>
              </form>
            </Box>
          </Center>
        </Box>
      </Flex>
    </>
  );
};

export default Registration;
