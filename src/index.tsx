import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Layout from "./components/Layout/Layout";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import Index from "./views/Index";
import Login from "./views/Login";
import Registration from "./views/Register";

import { ChakraProvider } from "@chakra-ui/react";
import ForgotPassword from "./views/ForgotPassword";
import Logout from "./views/Logout";

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider>
      <Router>
        <Layout>
          <Switch>
            <Route path="/" exact component={Index} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Registration} />
            <Route path="/forgotPassword" component={ForgotPassword} />
            <Route path="/logout" component={Logout} />
          </Switch>
        </Layout>
      </Router>
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
